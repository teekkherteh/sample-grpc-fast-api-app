# sample-grpc-fast-api-app

## Getting started

install gRPC

```bash
python -m pip install grpcio grpcio-tools
```

create virtual environment

```bash
python -m venv venv
--------or---------
python -m pip install virtualenv
virtualenv venv
-------------------
source venv/Scripts/activate
```

```bash
pip install grpcio grpcio-tools
```

## Start Fast API server

`main.py` will use `uvicorn` to start FastAPI server with port 8001. Try it out in `http://127.0.0.1:8001/docs`

```bash
python app/main.py
```

## Run a gRPC application

### Run gRPC server

```bash
python app/server.py
```

### from another terminal, run gRPC client

```bash
python app/client.py
```

Congratulations! You’ve just run a client-server application with gRPC.

### Generate gRPC code

update the gRPC code used by our application to use the new service definition.

```bash
cd app

python -m grpc_tools.protoc --proto_path=.  --python_out=. --grpc_python_out=. user/user.proto
```

## Support

NA

## Contributing

Thanks for everyone contribution. Comment and feedback are welcome.

## License

Copyright 2023 Eggplant
