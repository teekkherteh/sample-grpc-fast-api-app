
from concurrent import futures
import logging

import grpc
import user.user_pb2 as user_pb2
import user.user_pb2_grpc as user_pb2_grpc
import user.service as service


class UserService(user_pb2_grpc.UserServiceServicer):

    def GetUsers(self, request, context):
        user_list = user_pb2.UserReplyList()

        for uuu in service.get_users():
            user_list.user_reply_list.append(user_pb2.UserReply(
                id=uuu['id'],
                username=uuu['username'],
                email=uuu['email'],
                permission=uuu['permission'],
                password=uuu['password']
            ))

        print(user_list)
        return user_list

def serve():
    port = '50001'
    server = grpc.server(futures.ThreadPoolExecutor(max_workers=10))
    user_pb2_grpc.add_UserServiceServicer_to_server(UserService(), server)
    server.add_insecure_port('[::]:' + port)
    server.start()
    print("Server started, listening on " + port)
    server.wait_for_termination()


if __name__ == '__main__':
    logging.basicConfig()
    serve()