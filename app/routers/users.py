from http.client import HTTPException
from typing import List
from fastapi import APIRouter

from user.models import User
from user import service

router = APIRouter()

@router.get('/', response_model=List[User])
async def index():
    return service.get_users()

@router.post('/', status_code=201)
async def add_user(payload: User):
    return service.add_user(payload)

@router.put('/{id}')
async def update_user(id: str, payload: User):
    if id == None:
        raise HTTPException(status_code=404, detail="User with given id not found")

    service.update_user(id, payload)

@router.delete('/{id}')
async def delete_user(id: str):
    if id == None:
        raise HTTPException(status_code=404, detail="User with given id not found")

    service.delete_user(id)

