from fastapi import FastAPI
import uvicorn

import routers.users

app = FastAPI()

app.include_router(routers.users.router)

uvicorn.run(app, host="0.0.0.0", port=int(8001))
