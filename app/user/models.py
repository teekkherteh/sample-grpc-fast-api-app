from typing import List
from pydantic import BaseModel

class User(BaseModel):
    id: str
    username: str
    email: str
    permission: List[str]
    password: str