
from user.models import User


sample_users_db = [
    {
        'id' : 'e3d240c6-a8f5-11ed-afa1-0242ac120002',
        'username': 'james gunn',
        'email': 'james.gunn@dcentertainment.com',
        'password': 'just for testing',
        'permission': ['nothing1', 'nothing2']
    }
]

def get_users():
    return sample_users_db

def add_user(payload: User):
    user = payload.dict()
    sample_users_db.append(user)
    return {'id': len(sample_users_db) - 1}

def update_user(id: int, payload: User):
    user = payload.dict()
    users_length = len(sample_users_db)
    if 0 <= id <= users_length:
        sample_users_db[id] = user
        return None

def delete_user(id: int):
    users_length = len(sample_users_db)
    if 0 <= id <= users_length:
        del sample_users_db[id]
        return None