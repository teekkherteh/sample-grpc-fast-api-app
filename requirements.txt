fastapi==0.91.0
protobuf==4.21.12
pydantic==1.10.4
uvicorn==0.20.0
grpcio==1.51.1